# se importan para poder generar las respuestas http
from rest_framework.response import Response
# se importa para poder consumir las vistas de ayuda 
from rest_framework.views import APIView
# se importa para poder modificar las vistas de ayuda
from rest_framework.decorators import api_view
#se importa para poder usar el modelo creado
from productApp.models import Product
# se importa para poder manipular el modelo con el serializador creado
from productApp.serializers import ProductSerializer

#se agrega el modificador a la funcion para restringir los metodos http permitidos
#@api_view(['GET','POST', 'DELETE', 'PUT'])

@api_view(['GET'])
def listAll(request):
    product = Product.objects.all()
    product_serializer = ProductSerializer(product, many=True)
    return Response(product_serializer.data)

@api_view(['GET'])
def listDetail(request, pk):
    product = Product.objects.get(id = pk)
    product_serializer = ProductSerializer(product,many=False)
    return Response(product_serializer.data)

@api_view(['POST', 'GET'])
def createDate(request):
    product_serializer = ProductSerializer(data = request.data)
    if product_serializer.is_valid():
        product_serializer.save()
    return Response(product_serializer.data)

@api_view(['PUT', 'GET'])
def updateDate(request, pk):
    product = Product.objects.get(id = pk)#.first()
    product_serializer = ProductSerializer(instance=product,data = request.data)
       
    if product_serializer.is_valid():
        product_serializer.save()
            # se responde la data enviada al solicitante
        return Response(product_serializer.data)
        # se envia un error si los datos no cumplen con el modelo
    return Response(product_serializer.errors)

@api_view(['DELETE'])
def deleteDate(request, pk):
    if request.method == 'DELETE':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        product = Product.objects.get(id = pk)
        # se borra el dato de la base de datos
        product.delete()
        # se envia mensaje al solicitante
        return Response("Eliminado..")





"""def product_api_view(request, pk=None):

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        #  usando ORM se realiza la consultaa la base de datos de todos los productos
        #product = Product.objects.all()
        product = Product.objects.filter(id = pk).first()
        # se representa la informacion solicitada en JSON y se marca many=true para asignar anidacion
        product_serializer = ProductSerializer(product,many=True)
        # se envida la respuesta serializada que se encuentra en data
        return Response(product_serializer.data)

    elif request.method == 'DELETE':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        product = Product.objects.all()
        # se borra el dato de la base de datos
        product.delete()
        # se envia mensaje al solicitante
        return Response("Eliminado..")

    # se filtran los mensajes enviados por POST
    elif request.method == 'POST':
        # se extraen los datos enviados en la peticion request
        product_serializer = ProductSerializer(data = request.data)
        # se validan los datos si cumplen las caracteristicas del modelo
        if product_serializer.is_valid():
            # se salvan los datos enviados en la base de datos
            product_serializer.save()
            # se responde la data enviada al solicitante
            return Response(product_serializer.data)
        # se envia un error si los datos no cumplen con el modelo
        return Response(product_serializer.errors)


    elif request.method == 'PUT':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        #product = Product.objects.filter(id = pk).first()

        product = Product.objects.all()
        # se serializa los datos recibidos para la manipulacion
        product_serializer = ProductSerializer(instance=product,data = request.data)
        # se validan los datos si cumplen las caracteristicas del modelo
        if product_serializer.is_valid():
            # se salvan los cambios en la base de datos
            product_serializer.save()
            # se responde la data enviada al solicitante
            return Response(product_serializer.data)
        # se envia un error si los datos no cumplen con el modelo
        return Response(product_serializer.errors)

#se agrega el modificador a la funcion para restringir los metodos http permitidos
@api_view(['GET','PUT','DELETE'])
def product_detail_view(request,pk=None):

    # se filtran los mensajes enviados por GET
    if request.method == 'GET':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        product = Product.objects.filter(id = pk).first()
       # product = Product.objects.all
        # se serializa los datos para la manipulacion
        product_serializer = ProductSerializer(product)
        # se envia respuesta al solicitante
        return Response(product_serializer.data)
    
    # se filtran los mensajes enviados por PUT
    elif request.method == 'PUT':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        product = Product.objects.filter(id = pk).first()
        # se serializa los datos recibidos para la manipulacion
        product_serializer = ProductSerializer(instance=product,data = request.data)
        # se validan los datos si cumplen las caracteristicas del modelo
        if product_serializer.is_valid():
            # se salvan los cambios en la base de datos
            product_serializer.save()
            # se responde la data enviada al solicitante
            return Response(product_serializer.data)
        # se envia un error si los datos no cumplen con el modelo
        return Response(product_serializer.errors)
    
    # se filtran los mensajes enviados por PUT
    elif request.method == 'DELETE':
        # usando ORM se realiza la consultaa la base de datos y se filtra por el pk recibido
        product = Product.objects.filter(id = pk).first()
        # se borra el dato de la base de datos
        product.delete()
        # se envia mensaje al solicitante
        return Response("Eliminado..")"""

