# clase importada para creacion de modelo
from django.db import models

# clase creada para el modelo de producto
class Product(models.Model):
    #atributos del producto, que van a representar las columnas de la tabla
    id = models.AutoField(primary_key=True)
   # fecha_ingreso = models.CharField('fecha', max_length = 30)
    fecha_ingreso = models.DateField()
    numero_noches= models.IntegerField(default=0)
