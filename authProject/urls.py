from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from authApp import views
from productApp import views as viewsProduct


urlpatterns = [
    
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    
    path('getAll', viewsProduct.listAll, name="listAll"),
    path('getReserva/<str:pk>', viewsProduct.listDetail, name="listDetail"),
    path('createReserva/', viewsProduct.createDate, name="crearReserva"),
    path('updateReserva/<str:pk>', viewsProduct.updateDate, name="updateReserva"),
    path('deleteReserva/<str:pk>', viewsProduct.deleteDate, name="deleteReserva"),

]
